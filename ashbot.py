import logging

import config
import discord

# set up logging
logger = logging.getLogger('ashbot')
logger.setLevel(logging.DEBUG if config.DEBUG else logging.INFO)

formatter = logging.Formatter('[%(asctime)s][%(levelname)s][%(name)s]: %(message)s')
fhandler = logging.FileHandler(filename='ashbot.log', encoding='utf-8', mode='a')
fhandler.setFormatter(formatter)
logger.addHandler(fhandler)

if config.DEBUG:
    chandler = logging.StreamHandler()
    chandler.setLevel(logging.DEBUG)
    chandler.setFormatter(formatter)
    logger.addHandler(chandler)

# create bot
class AshBot(discord.Bot):
    async def on_ready(self):
        db = bot.get_cog('Database')
        if db is None:
            # TODO: make this a specific type of exception, make an `on_error` handler to catch and properly handle it (should prob be fatal)
            #       that same `on_error` handler will be nice for handling other event handler exceptions
            raise Exception('Database cog was not loaded! Consider looking into this before continuing to use the bot.')

        # ensure all joined guilds exist in guilds table
        db_guilds = await db.get_all_guild_ids()
        for guild in bot.guilds:
            if guild.id not in db_guilds:
                if not await db.initialize_guild(guild.id, check_exists=False):
                    logger.error(f'failed to add a guild entry for {guild.id}!')

        logger.info(f'Successfully logged in as {bot.user} (id: {bot.user.id})')

    # TODO: this always gets called, even if handled by cog_command_error, so move all error handling to cogs
    async def on_application_command_error(self, ctx: discord.ApplicationContext, error):
        logger.error(error)
        await super().on_application_command_error(ctx, error)
        # essentially a default error handler; is it better to re-raise? call super?
        # await ctx.respond(f'something went wrong while executing that command! `{error}`', ephemeral=True)

    # TODO: this also gets called for all errors, so need to find a way to have this for event listeners in cogs
    # async def on_error(self, event, *args, **kwargs):
    #     logging.error(f'exception raised in the {event} event: ({args}), ({kwargs})')
    #     await super().on_error(event, args, kwargs)

bot = AshBot(
    debug_guilds=config.debug_guilds,
    intents=discord.Intents.default(),
)

# load extensions
for cog in config.cogs:
    try:
        bot.load_extension(f'cogs.{cog}')
    except Exception as ex:
        logger.error(f'Failed to load extension "{cog}"! {ex.__class__.__name__}: {ex}')

# run bot
bot.run(config.token)
