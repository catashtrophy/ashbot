# ashbot

Ashbot is a custom discord bot written with py-cord, for me and my friends.

## Environment Variables

The following variables must be defined in a .env file for execution of this bot:

|Variable|Description|Required|
|--------|-----------|--------|
|DISCORD_TOKEN|Your bot's token for signing in|Yes|
|DISCORD_APP_ID|Your bot's app ID (honestly can't remember why this is here rn)|yes?|
|DISCORD_DEV_GUILD_ID|ID of the guild you wish to set as the "development" guild for the bot (application commands will update here "instantly")|No|
|DEBUG|Put the bot into debug mode when "true"|No|
