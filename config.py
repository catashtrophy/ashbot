import os

# Load environment variables
from dotenv import load_dotenv
load_dotenv()

DEBUG = os.environ['DEBUG'].lower() == 'true'
debug_guilds = [os.environ['DISCORD_DEV_GUILD_ID']] if DEBUG else None

token = os.environ['DISCORD_TOKEN']
app_id = os.environ['DISCORD_APP_ID']

# default cogs to load
cogs = [
    'AboutBot',
    'Admin',
    'Announcements',
    'ContextMenus',
    'Database',
    'FakeThing',
    'NSFWWebRequest',
    # 'Polls',
    'Random',
    'RandomReacts',
    'TextMod',
    'WebRequest',
    'WikiRequest',
]
