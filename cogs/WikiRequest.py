import logging
import random

import aiowiki
import discord
from discord.commands import Option, SlashCommandGroup, slash_command
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class WikiRequest(commands.Cog):
    '''Implements commands requesting information from various MediaWikis'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_command_error(self, ctx: discord.ApplicationContext, error):
        logger.error(f'WikiRequest error: {error}')
        if isinstance(error, commands.CommandOnCooldown):
            return await ctx.respond(f'you are trying to make that request too fast! please wait another {error.retry_after:.2f}s', ephemeral=True)
        await ctx.respond(f'something went wrong while executing that command! `{error}`', ephemeral=True)

    def generate_wiki_embed(self, title, summary, media, urls):
        embed = discord.Embed(
            color=discord.Color.random(),
            description=summary or 'No description available',
            title=title or 'No title available',
            url=urls.view if urls is not None else None,
        )
        if media is not None and len(media):
            # TODO: set image based on first to match r'.*\.(?:svg|jpg|jpeg|png|gif)$'
            # (^ might want to test separately first)
            embed.set_image(url=media[0])
        # TODO: if no image set, set a default WP logo thumbnail?
        return embed

    wikipedia = SlashCommandGroup('wikipedia', 'Commands that interface with Wikipedia.org')
    # TODO: add other wikis?

    @wikipedia.command()
    @commands.cooldown(rate=1, per=5)
    async def random(self, ctx:discord.ApplicationContext):
        '''Returns a random Wikipedia page.'''
        async with aiowiki.Wiki.wikipedia('en') as wiki:
            try:
                pages = await wiki.get_random_pages(5)
                if not len(pages):
                    return await ctx.respond('no random pages were returned! (pls tell the dev, as something is probably very wrong)', ephemeral=True)
                page = random.choice(pages)
            except Exception as ex:
                return await ctx.respond(f'failed to get a random wiki page! (error for the dev: "{ex}")', ephemeral=True)
            title = page.title
            try: summary = await page.summary()
            except: summary = None
            if not summary:
                summary = await page.text()[:2048]
            try: media = await page.media()
            except: media = None
            try: urls = await page.urls()
            except: urls = None

        embed = self.generate_wiki_embed(title, summary, media, urls)
        await ctx.respond(embed=embed)

    # TODO: a view-thingy for search results and navigating between them in the sent embed
    @wikipedia.command()
    @commands.cooldown(rate=1, per=5)
    async def search(
        self, ctx:discord.ApplicationContext,
        query: Option(str, 'Search query for Wikipedia'),
    ):
        '''Search wikipedia for a page.'''
        async with aiowiki.Wiki.wikipedia('en') as wiki:
            pages = await wiki.opensearch(query)
            if not len(pages):
                return await ctx.respond(f'no results found for "{query}"...')
            page = pages[0]
            title = page.title
            try: summary = await page.summary()
            except: summary = None
            if not summary:
                summary = await page.text()[:2048]
            try: media = await page.media()
            except: media = None
            try: urls = await page.urls()
            except: urls = None

        embed = self.generate_wiki_embed(title, summary, media, urls)
        await ctx.respond(embed=embed)

def setup(bot: discord.Bot):
    bot.add_cog(WikiRequest(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('WikiRequest')
