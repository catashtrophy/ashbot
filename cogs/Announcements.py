import logging
import random
from typing import Sequence

import discord
from discord.commands import SlashCommandGroup, Option, permissions
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class Announcements(commands.Cog):
    '''Implements announcements-related commands for bot moderators/server owners.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot
        # TODO: put titles in db and enable adding more?
        self.channel_create_titles = [
            'whoa, new channel alert!',
            'check out the new channel',
        ]
        self.channel_delete_titles = [
            'rip to the following channel',
            'a channel has been eliminated',
        ]
        self.emoji_create_titles = [
            'hot off the presses',
            'brand new epic emoji',
            'EMOJI GET',
        ]
        self.emoji_delete_titles = [
            'this emoji sucked. its gone now.',
            'rip emoji',
            'in memoriam',
        ]
        self.emoji_animated_text = [
            'this shit moves!! so fucking cool',
            'look at it go',
            'weeeeeeeeeee',
        ]
        self.sticker_create_titles = [
            'new sticker for the guild',
            'oooh, sticker!',
        ]
        self.sticker_delete_titles = [
            'goodbye sticker',
            'nobody even used this sticker anyway',
        ]

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        logger.warning(f'{ctx.author} executed priveleged announcements command: `{ctx.command.qualified_name}`')

        self.db = self.bot.get_cog('Database')
        if self.db is None:
            # TODO: just log this message, and let each command return a custom failure message to user like before? seems wasteful
            raise Exception('Database cog not loaded!')


    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel: discord.abc.GuildChannel):
        db = self.bot.get_cog('Database')
        if db is None:
            logger.error('Database cog not loaded for on_guild_channel_create!')
            return

        announcements_channel = await db.channel_create_announcements_enabled(channel.guild.id)
        if announcements_channel is None:
            return

        embed = discord.Embed(
            color=discord.Color.random(),
            description=channel.mention,
            title=random.choice(self.channel_create_titles),
            timestamp=channel.created_at,
        )
        await announcements_channel.send(embed=embed)

    @commands.Cog.listener()
    async def on_guild_channel_delete(self, channel: discord.abc.GuildChannel):
        db = self.bot.get_cog('Database')
        if db is None:
            logger.error('Database cog not loaded for on_guild_channel_delete!')
            return

        announcements_channel = await db.channel_delete_announcements_enabled(channel.guild.id)
        if announcements_channel is None:
            return

        embed = discord.Embed(
            color=discord.Color.random(),
            description=channel.name,
            title=random.choice(self.channel_delete_titles),
            timestamp=channel.created_at,
        )
        await announcements_channel.send(embed=embed)

    @commands.Cog.listener()
    async def on_guild_emojis_update(self, guild: discord.Guild, before: Sequence[discord.Emoji], after:Sequence[discord.Emoji]):
        db = self.bot.get_cog('Database')
        if db is None:
            logger.error('Database cog not loaded for on_guild_emojis_update!')
            return

        announcements_channel = await db.emoji_update_announcements_enabled(guild.id)
        if announcements_channel is None:
            return

        removals = list(set(before) - set(after))
        additions = list(set(after) - set(before))
        removed = len(removals) > 0
        added = len(additions) > 0

        if removed:
            emoji = removals.pop()
            title = random.choice(self.emoji_delete_titles)
        elif added:
            emoji = additions.pop()
            title = random.choice(self.emoji_create_titles)
        else:
            return

        embed = discord.Embed(
            color=discord.Color.random(),
            title=title,
            timestamp=emoji.created_at,
        )
        embed.set_image(url=emoji.url)
        embed.set_footer(text=emoji.name)
        if emoji.animated:
            embed.description = random.choice(self.emoji_animated_text)

        m = await announcements_channel.send(embed=embed)
        if added:
            await m.add_reaction(emoji=emoji)

    @commands.Cog.listener()
    async def on_guild_stickers_update(self, guild: discord.Guild, before: Sequence[discord.GuildSticker], after:Sequence[discord.GuildSticker]):
        db = self.bot.get_cog('Database')
        if db is None:
            logger.error('Database cog not loaded for on_guild_stickers_update!')
            return

        announcements_channel = await db.sticker_update_announcements_enabled(guild.id)
        if announcements_channel is None:
            return

        removals = list(set(before) - set(after))
        additions = list(set(after) - set(before))
        removed = len(removals) > 0
        added = len(additions) > 0

        if removed:
            sticker = removals.pop()
            titles = self.sticker_delete_titles
        elif added:
            sticker = additions.pop()
            titles = self.sticker_create_titles
        else:
            return

        embed = discord.Embed(
            color=discord.Color.random(),
            description=sticker.description,
            title=random.choice(titles),
            timestamp=sticker.created_at,
        )
        embed.set_image(url=sticker.url)
        embed.set_footer(text=sticker.name)
        await announcements_channel.send(embed=embed)

    # TODO: permissions rant, see Admin.py for better notes if you need
    # TODO: consider reorganizing these commands (once more yeah i know)
    announcements = SlashCommandGroup(
        'announcements',
        'Enable or disable announcements and announcement groups, manage annonucement channels',
        # permissions=[
        #     CommandPermission('mod', 1, True),
        #     CommandPermission('Mod', 1, True),
        #     CommandPermission('MOD', 1, True),
        #     CommandPermission('moderator', 1, True),
        #     CommandPermission('Moderator', 1, True),
        #     CommandPermission('MODERATOR', 1, True), # dont forget admin/op as well for completeness
        #     CommandPermission('owner', 2, True),
        # ],
        # default_permissions=False,
    )
    announcements_channel = announcements.create_subgroup('channel', 'Manage announcement channels for your guild')

    @announcements.command()
    async def enable(
        self, ctx: discord.ApplicationContext,
        channel: Option(discord.TextChannel, 'The default announcements channel you would like to have announcements sent to.'),
    ):
        '''Enable announcements in general for the current guild.'''
        await self.db.enable_announcements(ctx.guild_id, channel.id)
        await ctx.respond(f'successfully enabled announcements for this guild! announcements channel: {channel.mention}', ephemeral=True)

    @announcements.command()
    async def disable(self, ctx: discord.ApplicationContext):
        '''Disables announcements in general for the current guild.'''
        await self.db.disable_announcements(ctx.guild_id)
        await ctx.respond('successfully disabled announcements for this guild.', ephemeral=True)

    @announcements.command()
    async def check(self, ctx: discord.ApplicationContext):
        '''Checks the status of announcements for your guild.'''
        global_channel = await self.db.announcements_enabled(ctx.guild_id)
        channel_create_channel = await self.db.channel_create_announcements_enabled(ctx.guild_id)
        channel_delete_channel = await self.db.channel_delete_announcements_enabled(ctx.guild_id)
        emoji_update_channel = await self.db.emoji_update_announcements_enabled(ctx.guild_id)
        sticker_update_channel = await self.db.sticker_update_announcements_enabled(ctx.guild_id)

        e = discord.Embed(
            color=discord.Color.random(),
            description=f'*for guild:* {ctx.guild.name}',
            title='Announcements status',
            timestamp=discord.utils.utcnow(),
        )

        e.add_field(
            name='Global Announcements',
            value=global_channel.mention if global_channel is not None else '**announcements** are disabled, entirely',
            inline=False,
        ).add_field(
            name='Channel Creations',
            value=channel_create_channel.mention if channel_create_channel is not None else '**channel_create** announcements are disabled',
        ).add_field(
            name='Channel Deletions',
            value=channel_delete_channel.mention if channel_delete_channel is not None else '**channel_delete** announcements are disabled',
        ).add_field(
            name='Emoji Updates',
            value=emoji_update_channel.mention if emoji_update_channel is not None else '**emoji_update** announcements are disabled',
        ).add_field(
            name='Sticker Updates',
            value=sticker_update_channel.mention if sticker_update_channel is not None else '**sticker_update** announcements are disabled',
        )

        await ctx.respond(embed=e, ephemeral=True)

    @announcements.command()
    async def set_group(
        self, ctx: discord.ApplicationContext,
        which: Option(
            str,
            'Announcement group you wish to set the status of',
            choices=['channel_create', 'channel_delete', 'emoji_update', 'sticker_update'],
        ),
        enable: Option(bool, 'True if the announcement group should be enabled, otherwise False'),
        channel_override: Option(
            discord.TextChannel,
            'Optional override announcements channel for the chosen announcement group.',
            required=False,
            default=None,
        ),
    ):
        '''Enables/disables the given announcement group, optionally setting the channel override.'''
        match which:
            case 'channel_create':
                if enable: await self.db.enable_channel_create_announcements(ctx.guild_id, channel_override)
                else: await self.db.disable_channel_create_announcements(ctx.guild_id)
            case 'channel_delete':
                if enable: await self.db.enable_channel_delete_announcements(ctx.guild_id, channel_override)
                else: await self.db.disable_channel_delete_announcements(ctx.guild_id)
            case 'emoji_update':
                if enable: await self.db.enable_emoji_update_announcements(ctx.guild_id, channel_override)
                else: await self.db.disable_emoji_update_announcements(ctx.guild_id)
            case 'sticker_update':
                if enable: await self.db.enable_sticker_update_announcements(ctx.guild_id, channel_override)
                else: await self.db.disable_sticker_update_announcements(ctx.guild_id)
            case _:
                raise Exception('something has gone terribly wrong')

        if enable:
            which_channel = which.split('_')[0]
            channel_set_text = f' also set the {which_channel} announcement override announcements channel to {channel_override.mention}' if channel_override is not None else ''
            await ctx.respond(f'successfully enabled the {which} announcement for your guild.{channel_set_text}', ephemeral=True)
        else:
            await ctx.respond(f'successfully disabled the {which} announcement for your guild.', ephemeral=True)

    @announcements_channel.command(name='set')
    async def chan_set(
        self, ctx: discord.ApplicationContext,
        channel: Option(discord.TextChannel, 'Channel you wish to send announcements to'),
    ):
        '''Set the main announcements channel for the guild.'''
        await self.db.set_announcement_channel(ctx.guild_id, channel.id)
        await ctx.respond(f'set the global announcements channel for this guild to {channel.mention}', ephemeral=True)

    @announcements_channel.command()
    async def get(self, ctx: discord.ApplicationContext):
        '''Gets the main announcements channel for the guild.'''
        channel = await self.db.get_announcement_channel(ctx.guild_id)
        await ctx.respond(f'the global announcements channel for this guild is {channel.mention}', ephemeral=True)

    @announcements_channel.command()
    async def set_override(
        self, ctx: discord.ApplicationContext,
        which: Option(
            str,
            'Announcement override channel you wish to set',
            choices=['channel', 'emoji', 'sticker'],
        ),
        channel: Option(discord.TextChannel, 'Channel you wish to send the specified announcement group to'),
    ):
        '''Sets the override announcement channel for the given announcement group for the guild.'''
        match which:
            case 'channel':
                await self.db.set_channel_announcement_override_channel(ctx.guild_id, channel.id)
            case 'emoji':
                await self.db.set_emoji_announcement_override_channel(ctx.guild_id, channel.id)
            case 'sticker':
                await self.db.set_sticker_announcement_override_channel(ctx.guild_id, channel.id)
            case _:
                raise Exception('something has gone terribly wrong')

        await ctx.respond(f'successfully set the **{which}** announcement channel override channel to {channel.mention}', ephemeral=True)

    @announcements_channel.command()
    async def unset_override(
        self, ctx: discord.ApplicationContext,
        which: Option(
            str,
            'Announcement override channel you wish to unset',
            choices=['channel', 'emoji', 'sticker'],
        ),
    ):
        '''Unsets the override announcement channel for the given announcement group for the guild.'''
        match which:
            case 'channel':
                await self.db.set_channel_announcement_override_channel(ctx.guild_id, None)
            case 'emoji':
                await self.db.set_emoji_announcement_override_channel(ctx.guild_id, None)
            case 'sticker':
                await self.db.set_sticker_announcement_override_channel(ctx.guild_id, None)
            case _:
                raise Exception('something has gone terribly wrong')

        await ctx.respond(f'successfully unset the **{which}** announcement channel override channel. those announcements will now go to the **global announcements channel**.', ephemeral=True)

    @announcements_channel.command()
    async def get_override(
        self, ctx: discord.ApplicationContext,
        which: Option(
            str,
            'Announcement override channel you wish to get',
            choices=['channel', 'emoji', 'sticker'],
        ),
    ):
        '''Gets the override announcement channel for the given announcement group for the guild.'''
        match which:
            case 'channel':
                channel = await self.db.get_channel_announcement_override_channel(ctx.guild_id)
            case 'emoji':
                channel = await self.db.get_emoji_announcement_override_channel(ctx.guild_id)
            case 'sticker':
                channel = await self.db.get_sticker_announcement_override_channel(ctx.guild_id)
            case _:
                raise Exception('something has gone terribly wrong')

        if channel is None:
            return await ctx.respond(f'the **{which}** announcement channel override channel is currently **unset**.', ephemeral=True)

        await ctx.respond(f'the **{which}** announcement channel override channel is currently set to {channel.mention}', ephemeral=True)

def setup(bot: discord.Bot):
    bot.add_cog(Announcements(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('Announcements')
