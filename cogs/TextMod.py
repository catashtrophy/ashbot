import random
import re

import discord
from discord.commands import Option, slash_command, message_command
from discord.ext import commands

class TextMod(commands.Cog):
    '''Implements simple text modification commands'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @slash_command()
    async def b(
        self, ctx: discord.ApplicationContext,
        message: Option(str, "Text to 🅱e 🅱'd")
    ):
        '''🅱️🅱️🅱️ 🅱️🅱️ 🅱️🅱️🅱️🅱️🅱️ 🅱️🅱️🅱️ 🅱️🅱️🅱️🅱️🅱️🅱️🅱️ 🅱️🅱️🅱️🅱️.'''
        # base strength replacement, replace all b's
        b_msg = message.replace('b', '🅱')

        # generate a strength value to further randomly replace letters with 🅱
        strength = random.uniform(0, 1)
        if strength > 0.8:
            # 20% to replace all p's
            b_msg = b_msg.replace('p', '🅱')
        
        if strength > 0.9:
            # 10% to additionally replace up to 1/4 of the chars 
            r_count = random.randint(1, len(b_msg) // 4)
            for _ in range(r_count):
                r_idx = random.randrange(0, len(b_msg))
                b_msg = b_msg[:r_idx] + '🅱' + b_msg[r_idx + 1:]

        await ctx.respond(b_msg)

    # TODO: make a more robust clapping algorithm, add as message command as well
    @slash_command()
    async def clap(
        self, ctx: discord.ApplicationContext,
        message: Option(str, 'TEXT👏YOU👏WANT👏CLAPPED')
    ):
        '''ADD 👏 SOME 👏 CLAPS 👏 TO 👏 YOUR 👏 MESSAGE 👏'''
        # is there a better way? maybe
        await ctx.respond(message.replace(' ', '👏') + '👏')

    @slash_command()
    async def macode(
        self, ctx: discord.ApplicationContext,
        message: Option(str, ':ZT: :ZE: :ZX: :ZT:')
    ):
        '''Encode your message in the :ZM: :ZA: :ZC: :ZC: :ZO: :ZD: :ZE:'''
        enc_msg = []
        for word in message.split(' '):
            enc_word = re.sub(r'([A-Za-z0-9])', r':Z\1: ', word)
            enc_msg.append(enc_word.rstrip().upper())

        await ctx.respond('\n'.join(enc_msg))

    def uwu_text(self, text):
        def replace_keep_case(pattern, repl, src):
            def func(match):
                if match.group().islower(): return repl.lower()
                if match.group().istitle(): return repl.title()
                if match.group().isupper(): return repl.upper()
                return repl
            return re.sub(pattern, func, src, flags=re.I)

        uwu = replace_keep_case(r'r', 'w', text)
        uwu = replace_keep_case(r'l', 'w', uwu)
        uwu = replace_keep_case(r'th\s', 'f ', uwu)
        uwu = replace_keep_case(r'\sth', ' d', uwu)
        uwu = replace_keep_case(r'you', 'yuw', uwu)
        return f'{uwu} uwu'

    @slash_command()
    async def uwu(
        self, ctx: discord.ApplicationContext,
        message: Option(str, 'yuww text hewe :3 uwu')
    ):
        '''makes youw message vewy cute :3'''
        await ctx.respond(self.uwu_text(message))

def setup(bot: discord.Bot):
    bot.add_cog(TextMod(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('TextMod')
