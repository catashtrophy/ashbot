import io
import json
import logging
import random
import re

import aiohttp
import discord
from discord.commands import Option, slash_command
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class WebRequest(commands.Cog):
    '''Implements various generic web requesting commands.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_command_error(self, ctx: discord.ApplicationContext, error):
        logger.error(f'WebRequest error: {error}')
        if isinstance(error, commands.CommandOnCooldown):
            return await ctx.respond(f'you are trying to make that request too fast! please wait another {error.retry_after:.2f}s', ephemeral=True)
        # TODO: catch other errors like from aiohttp here?
        await ctx.respond(f'something went wrong while executing that command! `{error}`', ephemeral=True)

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        self.client = aiohttp.ClientSession()

    async def cog_after_invoke(self, ctx: discord.ApplicationContext):
        await self.client.close()

    def generate_image_embed(self, url: str, title: str=None):
        e = discord.Embed(
            title=title,
            color=discord.Color.random(),
        )
        e.set_image(url=url)
        return e

    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def e926(
        self, ctx: discord.ApplicationContext,
        tags: Option(str, 'Each tag you want to add to your search query, **separated by space**')
    ):
        '''Gets a post from e926 that contains all of your searched tags.'''
        headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36' }
        params = {
            'tags': tags,
            'limit': 25,
        }

        async with self.client.get('https://e926.net/posts.json', headers=headers, params=params) as r:
            if r.status != 200:
                return await ctx.respond(f';w; sumting went wwong wif yuww wequest (error for the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.json()
            if not data['posts'] or not len(data['posts']):
                return await ctx.respond(f'no resuwts found fow `{tags.replace(" ", "`, `")}`', ephemeral=True) # TODO: make not ephemeral?
            result = random.choice(data['posts'])

        embed = discord.Embed(
            color=discord.Color.random(),
            description=result['description'],
            timestamp=discord.utils.parse_time(result['updated_at']),
            title=f"`{tags.replace(' ', '`, `')}`",
            url=f"https://e926.net/posts/{result['id']}?q={tags.replace(' ', '+')}",
        )
        embed.set_author(name=', '.join(result['tags']['artist']))
        embed.set_footer(text=f'score: {result["score"]["total"]}')
        embed.set_image(url=result['file']['url'])
        embed.set_thumbnail(url=result['preview']['url'])
        for tag_cat, tag_list in result['tags'].items():
            if tag_cat == 'artist': continue
            if not len(tag_list): continue
            joined_tags = ', '.join(tag_list)
            # stop loop when next embed breaks size limit
            if len(joined_tags) + len(embed) > 6000: break
            embed.add_field(name=f'{tag_cat.title()} tags', value=joined_tags, inline=False)

        await ctx.respond(embed=embed)

    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def gravestone(
        self, ctx: discord.ApplicationContext,
        line1: Option(str, 'The first line of the gravestone', default=''),
        line2: Option(str, 'The second line', default=''),
        line3: Option(str, 'The third line', default=''),
        line4: Option(str, 'The fourth line', default=''),
    ):
        '''Generates a gravestone with up to four lines of text of your choosing.'''
        params = { 'top1': line1, 'top2': line2, 'top3': line3, 'top4': line4 }

        async with self.client.get(f'http://www.tombstonebuilder.com/generate.php', params=params) as r:
            if r.status != 200:
                return await ctx.respond(f'i died on the way to getting your gravestone :c (error for the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.read()
            file = discord.File(
                io.BytesIO(data),
                filename='gravestone.png',
                description=f'Customized gravestone, dreamt up by {ctx.author}',
            )

        e = self.generate_image_embed('attachment://gravestone.png')
        await ctx.respond(embed=e, file=file)

    # TODO: work out how to possibly implement a re-roll, with cached previous results or smth. might involve views?
    # Based on https://github.com/joeyism/duckduckgo-images-api
    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def image(
        self,
        ctx: discord.ApplicationContext,
        query: Option(str, 'Whatever your heart desires to search for'),
    ):
        '''Searches DuckDuckGo Images for your query.'''
        base_url = 'https://duckduckgo.com/'
        headers = {
            'authority': 'duckduckgo.com',
            'accept': 'application/json, text/javascript, */* q=0.01',
            'sec-fetch-dest': 'empty',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-mode': 'cors',
            'referer': 'https://duckduckgo.com/',
            'accept-language': 'en-US,enq=0.9',
        }
        params = {
            'l': 'us-en',
            'o': 'json',
            'q': query,
            'f': ',,,',
            'p': '1',
            'v7exp': 'a',
        }

        async with self.client.post(base_url, data={'q': query}) as token_resp:
            if token_resp.status != 200:
                return await ctx.respond(f'instead of an image, i got an error! (tell the dev: "error requesting token, `HTTP {token_resp.status}`")', ephemeral=True)
            token_text = await token_resp.text()
            token_search = re.search(r'vqd=([\d-]+)\&', token_text, re.M | re.I)
            if not token_search:
                return await ctx.respond(f'instead of an image, i got an error! (tell the dev: "token was not contained in response")', ephemeral=True)
            params['vqd'] = token_search.group(1) # set token in params

        async with self.client.get(f'{base_url}i.js', headers=headers, params=params) as r:
            if r.status != 200:
                return await ctx.respond(f'instead of an image, i got an error! (tell the dev: "error requesting images, `HTTP {token_resp.status}`")', ephemeral=True)
            # needs to be done in 2 steps bc of how ddg returns the data as application/x-javascript for some reason
            data = await r.text()
            images = json.loads(data)
            if not len(images['results']):
                return await ctx.respond(f"unfortunately, i couldn\'t find anything for `{images['query']}`") # , ephemeral=True
            image = random.choice(images['results'])

        e = self.generate_image_embed(image['image'], title=images['query'])
        await ctx.respond(embed=e)

    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def inspire(self, ctx: discord.ApplicationContext):
        '''Generates an inspiring quote and image from http://inspirobot.me'''
        async with self.client.get('http://inspirobot.me/api', params={'generate': 'true'}) as r:
            if r.status != 200:
                return await ctx.respond(f'im having a little trouble getting inspired... (error for the dev: "`HTTP {r.status}`")', ephemeral=True)
            link = await r.text()

        e = self.generate_image_embed(link)
        await ctx.respond(embed=e)

    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def reddit(
        self, ctx:discord.ApplicationContext,
        query: Option(str, 'Whatever you could possibly want to find on this godforsaken website'),
        sort_type: Option(
            str, 'How to sort posts when searching',
            required=False,
            default='hot',
            autocomplete=discord.utils.basic_autocomplete(['hot', 'top', 'new', 'relevance'])
        ),
    ):
        '''Searches reddit for whatever your heart desires'''
        if len(query) > 512:
            return await ctx.respond(f'your search query is too long! (must be less than 512 chars; yours: {len(query)} chars)', ephemeral=True)
        base_url = 'https://www.reddit.com/'
        params = {
            'q': query,
            'sort': sort_type,
            'limit': 2,
            'restrict_sr': 'false',
            'raw_json': 1,
        }

        async with self.client.get(f'{base_url}search.json', params=params) as r:
            if r.status != 200:
                return await ctx.respond(f'i found an error while searching reddit, that can\'t be right... (error for the dev: "`HTTP {r.status}`")')
            raw_data = await r.json()
            posts = list(map(lambda d: d['data'], raw_data['data']['children']))
            if not len(posts):
                return await ctx.respond(f'sorry, there were no results found for "`{query}`"!') #, ephemeral=True
            post = random.choice(posts)

        embed = discord.Embed(
            color=discord.Color.random(),
            description=post['selftext'][:2048] if post['is_self'] else f"[permalink]({base_url[:-1]}{post['permalink']})",
            timestamp=discord.utils.parse_time(post['created_utc']),
            title=f"{post['subreddit_name_prefixed']} - {post['title'][:253 - len(post['subreddit_name_prefixed'])]}",
            url=f"{base_url[:-1]}{post['permalink']}" if post['is_self'] else post['url'],
        )
        embed.set_author(name=post['author'], url=f"{base_url}u/{post['author']}")
        embed.set_footer(text=f"{post['score']} points")
        if re.match(r'(?:i\.)?(?:imgur|redd|gfy|giphy)', post['domain'], re.I):
            embed.set_image(url=post['url'])
        elif re.match(r'^https?:\/\/[^\s$.?#].[^\s]*$', post['thumbnail'], re.I | re.M):
            embed.set_thumbnail(url=post['thumbnail'])
        await ctx.respond(embed=embed)

    # TODO: that pagination view thing that you keep talking about
    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def urban(
        self, ctx: discord.ApplicationContext,
        query: Option(str, 'The phrase you would like to search for'),
    ):
        '''Searches urbandictionary.com for a phrase and returns the closest listing.'''
        async with self.client.get('http://api.urbandictionary.com/v0/define', params={'term': query}) as r:
            if r.status != 200:
                return await ctx.respond(f'an error occurred while performing that search! (tell the dev the error you got was "`HTTP {r.status}`")', ephemeral=True)
            data = await r.json()
            result_list = data['list']
            if not len(result_list):
                return await ctx.respond(f'no results found for "{query}"!')
            result = result_list[0]

        embed = discord.Embed(
            color=discord.Color.random(),
            title=f'"{result["word"]}"',
            timestamp=discord.utils.parse_time(result['written_on'][:-1]),
            url=result['permalink'],
        )
        embed.set_author(name=result['author'])
        embed.set_thumbnail(url='https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/UD_logo-01.svg/512px-UD_logo-01.svg.png')
        embed.set_footer(text=f"{result['thumbs_up']} 👍, {result['thumbs_down']} 👎")
        embed.add_field(name='Definition', value=result['definition'], inline=False)
        embed.add_field(name='Example', value=result['example'], inline=False)
        await ctx.respond(embed=embed)

    # TODO: a view or w/e would also be good here
    # TODO: could break out the search bit to a function, such that it can be reused for a potential `play` command? hehe 
    # Based on https://github.com/joetats/youtube_search
    @slash_command()
    @commands.cooldown(rate=1, per=5)
    async def youtube(
        self, ctx: discord.ApplicationContext,
        query: Option(str, 'What to search for on YouTube'),
    ):
        '''Search for a video on YouTube'''
        base_url = 'https://youtube.com'

        async with self.client.get(f'{base_url}/results', params={ "search_query": query }) as r:
            if r.status != 200:
                return await ctx.respond(f'failed to query youtube! (error for the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.text()
            if 'ytInitialData' not in data:
                return await ctx.respond(f'initial data not contained in response data! please tell the dev, something is probably fucked', ephemeral=True)

        # html parsing
        start = data.index('ytInitialData') + len('ytInitialData') + 3
        end = data.index('};', start) + 1
        json_str = data[start:end]
        json_data = json.loads(json_str)
        video_list = json_data['contents']['twoColumnSearchResultsRenderer']['primaryContents']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents']
        results = []
        for v in video_list:
            if 'videoRenderer' not in v.keys(): continue
            v_data = v.get('videoRenderer', {})
            results.append({
                'id': v_data.get('videoId', None),
                'thumbnails': [thumb.get('url', None) for thumb in v_data.get('thumbnail', {}).get('thumbnails', [{}])],
                'title': v_data.get('title', {}).get('runs', [[{}]])[0].get('text', None),
                'long_desc': v_data.get('descriptionSnippet', {}).get('runs', [{}])[0].get('text', None),
                'channel': v_data.get('longBylineText', {}).get('runs', [[{}]])[0].get('text', None),
                'duration': v_data.get('lengthText', {}).get('simpleText', 0),
                'views': v_data.get('viewCountText', {}).get('simpleText', 0),
                'publish_time': v_data.get('publishedTimeText', {}).get('simpleText', 0),
                'url_suffix': v_data.get('navigationEndpoint', {}).get('commandMetadata', {}).get('webCommandMetadata', {}).get('url', None),
            })

        if not len(results):
            return await ctx.respond(f'i got no results for "`{query}`"...') # , ephemeral=True
        video = random.choice(results[:10]) # only select from first 10 results

        embed = discord.Embed(
            color=discord.Color.random(),
            description=video['long_desc'],
            title=video['title'],
            url=f"{base_url}{video['url_suffix']}",
        )
        embed.set_author(name=video['channel'])
        embed.set_footer(text=f"duration: {video['duration']}, {video['views']}, {video['publish_time']}")
        embed.set_thumbnail(url=video['thumbnails'][0])
        await ctx.respond(embed=embed)

def setup(bot: discord.Bot):
    bot.add_cog(WebRequest(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('WebRequest')
