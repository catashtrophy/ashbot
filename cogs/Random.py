import hashlib
import random

import discord
from discord.commands import Option, slash_command
from discord.ext import commands

class Random(commands.Cog):
    '''Implements RNG-based commands.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @slash_command()
    async def based(
        self, ctx:discord.ApplicationContext,
        thing: Option(str, 'The thing'),
    ):
        '''Determines whether the thing is based or cringe.'''
        old_state = random.getstate()
        new_seed = hashlib.md5(thing.encode('utf-8')).hexdigest()
        random.seed(new_seed)

        verb = 'are' if thing.endswith('s') else 'is'
        basedness = random.uniform(0, 1)
        result = 'based' if basedness >= 0.5 else 'cringe'

        random.setstate(old_state)

        await ctx.respond(f'according to my calculations, "{thing}" {verb} {basedness * 100:.0f}% **{result}**')

    # TODO: make a modal thing for inputting list?
    @slash_command()
    async def choose(
        self, ctx: discord.ApplicationContext,
        items: Option(str, 'List of items you wish to have chosen from, seperated by comma (e.g.: cats, rats, bats)'),
    ):
        '''Picks something out of a list of items.'''
        item = random.choice(items.split(','))
        hmm = f"h{'m' * random.randint(3, 8)}{'.' * random.randint(3, 5)}"
        await ctx.respond(f'{hmm} i choose **{item.strip()}**!')

    @slash_command(name='8ball')
    async def eightball(
        self, ctx: discord.ApplicationContext,
        question: Option(str, 'The question you wish to ask the magic 8-ball'),
    ):
        '''Ask the magic 8-ball a question.'''
        # TODO: make cuter?
        response = random.choice([
            'it is certain.',
            'it is decidedly so.',
            'without a doubt.',
            'yes, definitely.',
            'you may rely on it.',
            'as i see it, yes.',
            'most likely.',
            'the outlook is good.',
            'yes.',
            'umm hell yeah!!',
            'reply hazy, try again.',
            'ask again later.',
            'better not tell you now...',
            'cannot predict now.',
            'maybe??? as if id know lmao',
            "don't count on it.",
            'my reply is no.',
            'my sources say no.',
            'very doubtful.',
            'fuuuck no.',
            'absolutely the fuck not.',
            'Grilled cheese',
        ])

        if not question.endswith('?'):
            question = question + '?'

        if question.lower() == 'am i stupid?':
            return await ctx.respond('\u22c5.\u22c5')

        await ctx.respond(f'"{question}", you ask?\n**{response}**')

    @slash_command()
    async def flip(self, ctx: discord.ApplicationContext):
        '''Flips a coin for you. The best tool for quickly settling disputes!'''
        face = random.choice(['heads', 'tails'])
        await ctx.respond(f'the coin landed **{face}** up.')

    @slash_command()
    async def rate(
        self, ctx: discord.ApplicationContext,
        thing: Option(str, 'Thing you want me to rate'),
    ):
        '''Rates something out of 10.'''
        overrate = random.uniform(0, 1) > 0.95
        rating = random.randint(1, 11 if overrate else 10)

        punctuation = '!' if rating > 6 else '.'
        if rating == 1: punctuation += '..'

        await ctx.respond(f'"{thing}"? i\'d rate that {rating}/10{punctuation}')

    @slash_command()
    async def roll(
        self, ctx: discord.ApplicationContext,
        sides: Option(
            int, 'Number of sides the die has',
            min_value=1,
            default=20,
        ),
    ):
        '''Rolls an n-sided die; d20 by default.'''
        if sides <= 0:
            return await ctx.respond(f"you clearly don't understand how dice work. you can't have a {sides}-sided die.")

        roll = random.randint(1, sides)
        if roll == 1:
            await ctx.respond('*critical **failure**!* you rolled a **1**...')
        elif roll == sides:
            await ctx.respond(f'*critical **success**!* you rolled a **{sides}**~!')
        else:
            await ctx.respond(f'you rolled **{roll}**.')

def setup(bot: discord.Bot):
    bot.add_cog(Random(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('Random')
