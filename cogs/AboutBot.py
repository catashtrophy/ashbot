import logging
import random

import discord
from discord.commands import slash_command
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class AboutBot(commands.Cog):
    '''Implements general bot-related commands/things.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        self.db = self.bot.get_cog('Database')
        if self.db is None:
            logger.warning('failed to get Database cog before AboutBot cog invoke! not fatal, continuing')

    @slash_command()
    async def about(self, ctx: discord.ApplicationContext):
        '''Get more information and statistics about this bot.'''
        now = discord.utils.utcnow()

        e = discord.Embed(
            color=discord.Color.random(),
            timestamp=now,
            title='~about me~',
            url='https://gitlab.com/catashtrophy/ashbot',
        )
        e.set_thumbnail(url=ctx.bot.user.display_avatar.url)
        e.set_author(name=ctx.bot.user.display_name, icon_url=ctx.bot.user.display_avatar.url)
        ai = await ctx.bot.application_info()
        e.set_footer(text='created with 💛💜 by ash', icon_url=ai.owner.avatar.url)

        botday = ctx.bot.user.created_at
        life = now - botday
        if botday.month == now.month and botday.day == now.day:
            e.add_field(
                name='🎂 **it my birthday** 🎂',
                value=f'i just turned {life.days // 365}!',
                inline=False,
            )
        else:
            # TODO: better formatting for age, should automatically convert from days to months to years, etc.
            e.add_field(
                name='my age',
                value=f'it has been {life.days} days since my creation',
            )

        e.add_field(name='my pronouns!', value='it/its/itself')

        if self.db is not None:
            guild_goods, guild_bads, guild_thanks = await self.db.get_all_counts(ctx.guild_id)
            global_goods, global_bads, global_thanks = await self.db.get_all_counts(None)
            global_good_bad_sum = global_goods + global_bads
            if global_good_bad_sum != 0:
                good_percentage = global_goods / global_good_bad_sum
                bad_percentage = global_bads / global_good_bad_sum
                rating = max(good_percentage, bad_percentage) * 100.0
                is_good = good_percentage >= bad_percentage
                rating_text = 'good' if is_good else 'bad'
                flavor_text = 'by users like you! :3' if is_good else 'by... *sigh* **users**, like you...'
            else: 
                good_percentage = bad_percentage = 0.0
                rating = 0.0
                rating_text = '... wait... am i neither good nor bad?'
                flavor_text = 'i guess i am yet to be rated.'

            e.add_field(
                name='good or bad bot?',
                value=f'i am currently rated as {rating:.2f}% {rating_text} {flavor_text}',
                inline=False,
            )
            e.add_field(
                name='how good am i? 🥺',
                value=f'the kind folks here have called me good **{guild_goods}** time{"s" if guild_goods > 1 else ""}!',
            )
            e.add_field(
                name='how bad i am 😈',
                value=f'all of yall in this guild have told me im bad **{guild_bads}** time{"s" if guild_bads > 1 else ""}...',
            )
            e.add_field(
                name='thank me for my service 😇',
                value=f'i have received **{global_thanks}** total "thank you"{"s" if global_thanks > 1 else ""} across all the servers i inhabit, but this guild has gone out of its way to thank me **{guild_thanks}** time{"s" if guild_thanks > 1 else ""}! so, thank **you**!',
                inline=False,
            )

        await ctx.respond(embed=e, ephemeral=True)

    # TODO: for the following 3 commands, do anything with the returned count?
    #       could enable custom messages to be added via db, then 
    #       have a replacable token to represent current count for each in response message
    @slash_command()
    async def bad(self, ctx: discord.ApplicationContext):
        '''Reprimand the bot.'''
        if self.db is not None:
            count = await self.db.increment_bad_count(ctx.guild_id)

        await ctx.respond(random.choice([
            ':c',
            'ok :(',
            'oof',
            'understandable...',
            'w-why? 3:',
            'wow',
            'i promise ill be better!!',
        ]))

    @slash_command()
    async def good(self, ctx: discord.ApplicationContext):
        '''Let the bot know that it did a good :3'''
        if self.db is not None:
            count = await self.db.increment_good_count(ctx.guild_id)

        await ctx.respond(random.choice([
            'thank :3',
            'c:',
            'aww thanks!!',
            'well, thank you very much :)',
            'does this mean i get to take a break now? >:3',
        ]))

    @slash_command()
    async def thank(self, ctx: discord.ApplicationContext):
        '''Thank the bot for its work o7'''
        if self.db is not None:
            count = await self.db.increment_thank_count(ctx.guild_id)

        await ctx.respond(random.choice([
            'ur welc :3',
            'you are very welcome!!',
            'no prob!',
            'anything for you! 💖',
            "hehe you're welcome c:",
        ]))

def setup(bot: discord.Bot):
    bot.add_cog(AboutBot(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('AboutBot')
