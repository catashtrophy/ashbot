import io
import logging
import random

import discord
from discord.commands import message_command, user_command
from discord.ext import commands
from wordcloud import WordCloud

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class ContextMenus(commands.Cog):
    '''Implements various context menu commands.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @user_command(name='Slap!')
    async def slap(self, ctx: discord.ApplicationContext, member: discord.Member):
        '''Take your anger out over the internet.'''
        db = self.bot.get_cog('Database')
        if db is None:
            logger.error('Database cog not loaded for ContextMenus!')
            return await ctx.respond('i cant seem to access my database right now, sorry :c (please let the dev know!)', ephemeral=True)

        slap = await db.get_slap(for_bot=member.bot)
        if slap is None:
            return await ctx.respond('there are no slaps configured! let the bot creator know to add some, so you can use this feature.', ephemeral=True)

        user = ctx.author.mention
        target = 'themself' if member.id == ctx.author.id else member.mention
        await ctx.respond(slap.format(user=user, target=target))

    @message_command(name='UwU this')
    async def uwu_this(self, ctx: discord.ApplicationContext, message: discord.Message):
        '''makes the sewected message vewy cute :3'''
        text_mod = self.bot.get_cog('TextMod')
        if text_mod is None:
            logger.error(f'TextMod cog not loaded for ContextMenus cog! (trying to uwu_this a message (id: {message.id}) in guild id: {ctx.guild_id})')
            return await ctx.respond('failed to load text modification cog! sowwy :c (pwease teww de dev)', ephemeral=True)
        await ctx.respond(text_mod.uwu_text(message.content))

    @message_command(name='Wordcloud this')
    async def wordcloud(self, ctx: discord.ApplicationContext, message: discord.Message):
        '''Creates a wordcloud from the content of the selected message.'''
        # TODO: color modifications and shit to make them prettier
        wordcloud = WordCloud().generate(message.content)

        wc_image = wordcloud.to_image()
        data = io.BytesIO()
        wc_image.save(data, format='png', optimize=True)
        data.seek(0)
        file = discord.File(
            data,
            filename='wordcloud.png',
            description=f'Wordcloud generated from message by {message.author}',
        )

        embed = discord.Embed(
            color=discord.Color.random(),
            timestamp=message.created_at,
        )
        embed.set_author(name=message.author.display_name, icon_url=message.author.avatar.url)
        embed.set_image(url='attachment://wordcloud.png')
        await ctx.respond(embed=embed, file=file)

def setup(bot: discord.Bot):
    bot.add_cog(ContextMenus(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('ContextMenus')
