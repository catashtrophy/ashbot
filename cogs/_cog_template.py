import logging

import discord
from discord.commands import Option, slash_command
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class CogName(commands.Cog):
    '''Implements '''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @slash_command()
    async def cogmmand(self, ctx: discord.ApplicationContext):
        '''description'''
        await ctx.respond(f'')

def setup(bot: discord.Bot):
    bot.add_cog(CogName(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('')
