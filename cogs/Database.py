import logging
import sqlite3
from typing import Sequence, Optional, Tuple

import aiosqlite
import discord
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class Database(commands.Cog):
    '''Implements database stuff for ashbot.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_guild_join(self, guild: discord.Guild):
        init = await self.initialize_guild(guild.id)
        if init is None:
            logger.info(f'guild entry already found for {guild.id} in guilds table.')
        elif not init:
            logger.error(f'failed to add a guild entry for {guild.id}!')

    # TODO: cog_before_invoke and _after_invoke for setting up db connection and closing? would save some indents and repeated lines
    #       or some equivalent that works in general for cog, not just commands. these would only execute before commands
    #       ^ this dream may be dead idk, may not even matter all that much

    # TODO: will want a custom error handler for this entire cog, i think thats supported through pycord?
    #       important for catching various sqlite3 exceptions/errors, esp in the potential case of inserts into guilds
    #       another case i can see is the edge case of a guild not existing in the guilds table at runtime, which would cause most if not all selects to throw errors
    # TODO: tear this out and replace with one in Ashbot class, this is a global handler (everything shows up as db error lmao)
    # @commands.Cog.listener()
    # async def on_application_command_error(self, ctx: discord.ApplicationContext, error):
    #     # TODO: catch specific sqlite errors and handle them more carefully
    #     logger.error(f'db error occurred: {error}')

    """Guild existence/initialization"""

    async def get_all_guild_ids(self):
        '''Returns a list of all guild_ids in guilds table'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('select guild_id from guilds') as cur:
                rows = await cur.fetchall()
                return [row[0] for row in rows]

    async def guild_exists(self, guild_id: int) -> bool:
        '''Return true if the guild_id exists in the guilds table. Else, false.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT COUNT(*) FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                return row[0] == 1

    async def initialize_guild(self, guild_id: int, check_exists: bool=True) -> Optional[bool]:
        """Inserts an initialized guild row into the guilds table for the given guild_id, if it doesn't already exist. Returns true on success."""
        if check_exists:
            if await self.guild_exists(guild_id):
                return None
            else:
                logger.info(f'no guilds table entry found for {guild_id}, creating...')

        async with aiosqlite.connect('ashbot.db') as db:
            cur = await db.execute("""INSERT INTO guilds VALUES
            (?, 0, 0, 0, NULL, 'N', 'N', 'N', 'N', 'N', NULL, NULL, NULL, 'N', 0.0)""", (guild_id,))
            if cur.rowcount == 1:
                await db.commit()
                await cur.close()
                return True
            else:
                await db.rollback()
                await cur.close()
                return False

    """Counter incrementation and inquiry"""

    async def increment_good_count(self, guild_id: int) -> int:
        '''Increments the good_count for the given guild in the guilds table, returns the new count.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET good_count = good_count + 1 WHERE guild_id = ?', (guild_id,))
            await db.commit()
            async with db.execute('SELECT good_count FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                return row[0]

    async def increment_bad_count(self, guild_id: int) -> int:
        '''Increments the bad_count for the given guild in the guilds table, returns the new count.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET bad_count = bad_count + 1 WHERE guild_id = ?', (guild_id,))
            await db.commit()
            async with db.execute('SELECT bad_count FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                return row[0]

    async def increment_thank_count(self, guild_id: int) -> int:
        '''Increments the thank_count for the given guild in the guilds table, returns the new count.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET thank_count = thank_count + 1 WHERE guild_id = ?', (guild_id,))
            await db.commit()
            async with db.execute('SELECT thank_count FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                return row[0]

    async def get_all_counts(self, guild_id: Optional[int]) -> Tuple[int, int, int]:
        '''Returns all counts, either for a specified guild or the sum of all guilds, as a tuple in the following order: (good, bad, thank)'''
        async with aiosqlite.connect('ashbot.db') as db:
            if guild_id is None:
                cur = await db.execute('SELECT SUM(good_count), SUM(bad_count), SUM(thank_count) FROM guilds')
            else:
                cur = await db.execute('SELECT good_count, bad_count, thank_count FROM guilds WHERE guild_id = ?', (guild_id,))
            row = await cur.fetchone()
            return row[0], row[1], row[2]

    """Announcement enablement checkers"""

    async def announcements_enabled(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the announcement channel for the given guild if announcements are enabled, else returns None.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("SELECT announcement_channel_id FROM guilds WHERE guild_id = ? AND UPPER(enable_announcements) = 'Y'", (guild_id,)) as cur:
                row = await cur.fetchone()
                if row is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid announcements channel set! (invalid channel id: {row[0]})')
                    return None

    async def channel_create_announcements_enabled(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the channel announcement override channel, or the standard announcement channel if no override is set. If either all announcements or this specific announcement are disabled, None is returned'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("""SELECT IFNULL(channel_announcement_override_channel_id,
                                                   announcement_channel_id)
                                       FROM guilds
                                      WHERE     guild_id = ?
                                            AND UPPER(enable_announcements) = 'Y'
                                            AND UPPER(enable_channel_create) = 'Y'""", (guild_id,)) as cur:
                row = await cur.fetchone()
                if row is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid announcements channel or channel override set for `channel_create`! (invalid channel id: {row[0]})')
                    return None

    async def channel_delete_announcements_enabled(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the channel announcement override channel, or the standard announcement channel if no override is set. If either all announcements or this specific announcement are disabled, None is returned'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("""SELECT IFNULL(channel_announcement_override_channel_id,
                                                   announcement_channel_id)
                                       FROM guilds
                                      WHERE     guild_id = ?
                                            AND UPPER(enable_announcements) = 'Y'
                                            AND UPPER(enable_channel_delete) = 'Y'""", (guild_id,)) as cur:
                row = await cur.fetchone()
                if row is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid announcements channel or channel override set for `channel_delete`! (invalid channel id: {row[0]})')
                    return None

    async def emoji_update_announcements_enabled(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the emoji announcement override channel, or the standard announcement channel if no override is set. If either all announcements or this specific announcement are disabled, None is returned'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("""SELECT IFNULL(emoji_announcement_override_channel_id,
                                                   announcement_channel_id)
                                       FROM guilds
                                      WHERE     guild_id = ?
                                            AND UPPER(enable_announcements) = 'Y'
                                            AND UPPER(enable_emoji_update) = 'Y'""", (guild_id,)) as cur:
                row = await cur.fetchone()
                if row is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid announcements channel or channel override set for `emoji_update`! (invalid channel id: {row[0]})')
                    return None

    async def sticker_update_announcements_enabled(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the sticker announcement override channel, or the standard announcement channel if no override is set. If either all announcements or this specific announcement are disabled, None is returned'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("""SELECT IFNULL(sticker_announcement_override_channel_id,
                                                   announcement_channel_id)
                                       FROM guilds
                                      WHERE     guild_id = ?
                                            AND UPPER(enable_announcements) = 'Y'
                                            AND UPPER(enable_sticker_update) = 'Y'""", (guild_id,)) as cur:
                row = await cur.fetchone()
                if row is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid announcements channel or channel override set for `sticker_update`! (invalid channel id: {row[0]})')
                    return None

    """Announcement enablers"""

    async def enable_announcements(self, guild_id: int, channel_id: int) -> None:
        '''Enables announcements for the given guild, setting the announcement_channel_id to the specified channel.
        Note that no actual announcements are enabled by default, and they must be manually enabled as desired.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_announcements = 'Y', announcement_channel_id = ? WHERE guild_id = ?", (channel_id, guild_id))
            await db.commit()

    async def enable_channel_create_announcements(self, guild_id: int, channel: Optional[discord.TextChannel]) -> None:
        '''Enables channel_create announcements for the given guild, optionally setting the channel_announcement_override_channel_id to a specified channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            if channel is None:
                await db.execute("UPDATE guilds SET enable_channel_create = 'Y' WHERE guild_id = ?", (guild_id,))
            else:
                await db.execute("UPDATE guilds SET enable_channel_create = 'Y', channel_announcement_override_channel_id = ? WHERE guild_id = ?", (channel.id, guild_id))
            await db.commit()

    async def enable_channel_delete_announcements(self, guild_id: int, channel: Optional[discord.TextChannel]) -> None:
        '''Enables channel_delete announcements for the given guild, optionally setting the channel_announcement_override_channel_id to a specified channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            if channel is None:
                await db.execute("UPDATE guilds SET enable_channel_delete = 'Y' WHERE guild_id = ?", (guild_id,))
            else:
                await db.execute("UPDATE guilds SET enable_channel_delete = 'Y', channel_announcement_override_channel_id = ? WHERE guild_id = ?", (channel.id, guild_id))
            await db.commit()

    async def enable_emoji_update_announcements(self, guild_id: int, channel: Optional[discord.TextChannel]) -> None:
        '''Enables emoji_update announcements for the given guild, optionally setting the emoji_announcement_override_channel_id to a specified channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            if channel is None:
                await db.execute("UPDATE guilds SET enable_emoji_update = 'Y' WHERE guild_id = ?", (guild_id,))
            else:
                await db.execute("UPDATE guilds SET enable_emoji_update = 'Y', emoji_announcement_override_channel_id = ? WHERE guild_id = ?", (channel.id, guild_id))
            await db.commit()

    async def enable_sticker_update_announcements(self, guild_id: int, channel: Optional[discord.TextChannel]) -> None:
        '''Enables sticker_update announcements for the given guild, optionally setting the sticker_announcement_override_channel_id to a specified channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            if channel is None:
                await db.execute("UPDATE guilds SET enable_sticker_update = 'Y' WHERE guild_id = ?", (guild_id,))
            else:
                await db.execute("UPDATE guilds SET enable_sticker_update = 'Y', sticker_announcement_override_channel_id = ? WHERE guild_id = ?", (channel, guild_id))
            await db.commit()

    """Announcement disablers"""

    async def disable_announcements(self, guild_id: int) -> None:
        '''Disables announcements for the given guild. Note, does not clear announcement_channel_id, since it must be set when enabling.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_announcements = 'N' WHERE guild_id = ?", (guild_id,))
            await db.commit()

    async def disable_channel_create_announcements(self, guild_id: int) -> None:
        '''Disables channel_create announcements for the given guild, does not touch associated override channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_channel_create = 'N' WHERE guild_id = ?", (guild_id,))
            await db.commit()

    async def disable_channel_delete_announcements(self, guild_id: int) -> None:
        '''Disables channel_delete announcements for the given guild, does not touch associated override channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_channel_delete = 'N' WHERE guild_id = ?", (guild_id,))
            await db.commit()

    async def disable_emoji_update_announcements(self, guild_id: int) -> None:
        '''Disables emoji_update announcements for the given guild, does not touch associated override channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_emoji_update = 'N' WHERE guild_id = ?", (guild_id,))
            await db.commit()

    async def disable_sticker_update_announcements(self, guild_id: int) -> None:
        '''Disables sticker_update announcements for the given guild, does not touch associated override channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_sticker_update = 'N' WHERE guild_id = ?", (guild_id,))
            await db.commit()

    """Announcement channel setters"""

    async def set_announcement_channel(self, guild_id: int, channel_id: int) -> None:
        '''Sets the announcement_channel_id for the given guild to the chosen channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET announcement_channel_id = ? WHERE guild_id = ?', (channel_id, guild_id))
            await db.commit()

    async def set_channel_announcement_override_channel(self, guild_id: int, channel_id: int) -> None:
        '''Sets the channel_announcement_override_channel_id for the given guild to the chosen channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET channel_announcement_override_channel_id = ? WHERE guild_id = ?', (channel_id, guild_id))
            await db.commit()

    async def set_emoji_announcement_override_channel(self, guild_id: int, channel_id: int) -> None:
        '''Sets the emoji_announcement_override_channel_id for the given guild to the chosen channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET emoji_announcement_override_channel_id = ? WHERE guild_id = ?', (channel_id, guild_id))
            await db.commit()

    async def set_sticker_announcement_override_channel(self, guild_id: int, channel_id: int) -> None:
        '''Sets the sticker_announcement_override_channel_id for the given guild to the chosen channel.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('UPDATE guilds SET sticker_announcement_override_channel_id = ? WHERE guild_id = ?', (channel_id, guild_id))
            await db.commit()

    """Announcement channel getters"""

    async def get_announcement_channel(self, guild_id: int) -> discord.abc.GuildChannel:
        '''Gets the set announcement channel for the given guild'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT announcement_channel_id FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                guild = await self.bot.fetch_guild(guild_id)
                channel = await guild.fetch_channel(row[0])
                return channel

    async def get_channel_announcement_override_channel(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the set channel announcement override channel for the given guild'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT channel_announcement_override_channel_id FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                if row[0] is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid channel announcements override channel set! (invalid channel id: {row[0]})')
                    return None

    async def get_emoji_announcement_override_channel(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the set emoji announcement override channel for the given guild'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT emoji_announcement_override_channel_id FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                if row[0] is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid emoji announcements override channel set! (invalid channel id: {row[0]})')
                    return None

    async def get_sticker_announcement_override_channel(self, guild_id: int) -> Optional[discord.abc.GuildChannel]:
        '''Gets the set sticker announcement override channel for the given guild'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT sticker_announcement_override_channel_id FROM guilds WHERE guild_id = ?', (guild_id,)) as cur:
                row = await cur.fetchone()
                if row[0] is None: return None
                guild = await self.bot.fetch_guild(guild_id)
                try:
                    return await guild.fetch_channel(row[0])
                except:
                    logger.error(f'guild id {guild_id} has invalid sticker announcements override channel set! (invalid channel id: {row[0]})')
                    return None

    """Random reacts related"""

    async def random_reacts_enabled(self, guild_id: int) -> Optional[int]:
        '''Returns the random_react_chance for the given guild if random reacts are enabled, else returns None.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("SELECT random_react_chance FROM guilds WHERE guild_id = ? AND UPPER(enable_random_reacts) = 'Y'", (guild_id,)) as cur:
                row = await cur.fetchone()
                return None if row is None else row[0]
    
    async def enable_random_reacts(self, guild_id: int, chance: Optional[float]=None) -> None:
        '''Enables random reacts for the given guild, optionally setting the random react chance as well.'''
        async with aiosqlite.connect('ashbot.db') as db:
            if chance is None:
                await db.execute("UPDATE guilds SET enable_random_reacts = 'Y' WHERE guild_id = ?", (guild_id,))
            else:
                await db.execute("UPDATE guilds SET enable_random_reacts = 'Y', random_react_chance = ? WHERE guild_id = ?", (chance, guild_id))

            await db.commit()

    async def disable_random_reacts(self, guild_id: int) -> None:
        '''Disables random reacts for the given guild.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET enable_random_reacts = 'N' WHERE guild_id = ?", (guild_id,))
            await db.commit()

    async def set_random_reacts_chance(self, guild_id: int, chance: float) -> None:
        '''Sets the random react chance for the given guild to a float between 0 and 1.'''
        if 0 > chance > 1:
            raise Exception('Random react chance cannot be outside range [0.0, 1.0]')

        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute("UPDATE guilds SET random_react_chance = ? WHERE guild_id = ?", (chance, guild_id))
            await db.commit()

    async def add_random_reaction(self, guild_id: int, user_id: int, reaction: str) -> None:
        '''Adds a random reaction for the given user in the given guild.'''
        async with aiosqlite.connect('ashbot.db') as db:
            await db.execute('INSERT INTO random_reacts VALUES (?, ?, ?)', (guild_id, user_id, reaction))
            await db.commit()

    async def get_random_reactions(self, guild_id: int, user_id: int) -> Sequence[str]:
        '''Gets all random reactions stored for the given user in the given guild.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT reaction FROM random_reacts WHERE guild_id = ? AND user_id = ?', (guild_id, user_id)) as cur:
                rows = await cur.fetchall()
                return [row[0] for row in rows]

    async def remove_random_reaction(self, guild_id: int, user_id: int, reaction: str) -> bool:
        '''Removes a random reaction for the given user in the given guild.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('DELETE FROM random_reacts WHERE guild_id = ? AND user_id = ? AND reaction = ?', (guild_id, user_id, reaction)) as cur:
                if cur.rowcount != 1:
                    return False
                await db.commit()
                return True

    """Slap weapons related"""

    async def add_slap_weapon(self, weapon: str, action: Optional[str]=None, flavor_text: Optional[str]=None, for_bots: Optional[bool]=False) -> Optional[int]:
        '''Adds the specified slap weapon to the slap_weapons table. Returns the id of the added weapon on success, None on failure.'''
        async with aiosqlite.connect('ashbot.db') as db:
            sql_str = f"""INSERT INTO slap_weapons
            (weapon{', action' if action else ''}{', flavor_text' if flavor_text else ''}{', for_bots' if for_bots else ''})
            VALUES (?{', ?' if action else ''}{', ?' if flavor_text else ''}{', ?' if for_bots else ''})"""

            # TODO: HELP IDK HOW TO TUPLE THERE HAS TO BE A BETTER WAY THAN THIS LMFAOOOOOO
            if action and flavor_text and for_bots:
                subs = (weapon, action, flavor_text, 'Y')
            if action and flavor_text and not for_bots:
                subs = (weapon, action, flavor_text)
            if action and not flavor_text and for_bots:
                subs = (weapon, action, 'Y')
            if action and not flavor_text and not for_bots:
                subs = (weapon, action)
            if not action and flavor_text and for_bots:
                subs = (weapon, flavor_text, 'Y')
            if not action and flavor_text and not for_bots:
                subs = (weapon, flavor_text)
            if not action and not flavor_text and for_bots:
                subs = (weapon, 'Y')
            if not action and not flavor_text and not for_bots:
                subs = (weapon,)

            async with db.execute(sql_str, subs) as cur:
                if cur.rowcount != 1:
                    return None

                await db.commit()
                async with db.execute('SELECT MAX(id) FROM slap_weapons') as id_cur:
                    added_id = await id_cur.fetchone()
                    return added_id[0]

    async def get_slap_weapons(self) -> Sequence[tuple[int, str, str, str, str]]:
        '''Returns a list of all slap weapons'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT * FROM slap_weapons') as cur:
                rows = await cur.fetchall()
                return rows

    async def get_slap_weapon_ids(self) -> Sequence[tuple[int,]]:
        '''Returns a list of all slap weapon ids'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('SELECT id FROM slap_weapons') as cur:
                rows = await cur.fetchall()
                return rows

    async def get_slap(self, for_bot: bool=False) -> Optional[str]:
        '''Gets a random slap string using data from the slap weapons table. Format with {user} and {target}.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute("""SELECT printf('{user} %s {target}%swith **%s**!',
                                                   action,
                                                   IIF(flavor_text IS NULL,
                                                       ' ',
                                                       printf(' %s ', flavor_text)),
                                                   weapon)
                                       FROM slap_weapons
                                      WHERE for_bots = ?
                                   ORDER BY RANDOM() LIMIT 1""", ('Y' if for_bot else 'N',)) as cur:
                row = await cur.fetchone()
                return row[0] if row is not None else None

    async def remove_slap_weapon(self, id: int) -> bool:
        '''Removes the slap weapon from the slap_weapons table with the given id. Returns True on success.'''
        async with aiosqlite.connect('ashbot.db') as db:
            async with db.execute('DELETE FROM slap_weapons WHERE id = ?', (id,)) as cur:
                if cur.rowcount != 1:
                    return False
                await db.commit()
                return True

def setup(bot: discord.Bot):
    with sqlite3.connect('ashbot.db') as db:
        db.execute('''CREATE TABLE IF NOT EXISTS guilds 
                      (guild_id                                   INTEGER PRIMARY KEY,
                       good_count                                 INTEGER,
                       bad_count                                  INTEGER,
                       thank_count                                INTEGER,
                       announcement_channel_id                    INTEGER,
                       enable_announcements                       TEXT,
                       enable_channel_create                      TEXT,
                       enable_channel_delete                      TEXT,
                       enable_emoji_update                        TEXT,
                       enable_sticker_update                      TEXT,
                       channel_announcement_override_channel_id   INTEGER,
                       emoji_announcement_override_channel_id     INTEGER,
                       sticker_announcement_override_channel_id   INTEGER,
                       enable_random_reacts                       TEXT,
                       random_react_chance                        REAL)''')

        db.execute('''CREATE TABLE IF NOT EXISTS random_reacts
                      (guild_id   INTEGER,
                       user_id    INTEGER,
                       reaction   TEXT NOT NULL)''')

        db.execute("""CREATE TABLE IF NOT EXISTS slap_weapons
                      (id            INTEGER PRIMARY KEY,
                       weapon        TEXT NOT NULL,
                       action        TEXT DEFAULT 'slapped',
                       flavor_text   TEXT,
                       for_bots      TEXT DEFAULT 'N')""")

    bot.add_cog(Database(bot))
