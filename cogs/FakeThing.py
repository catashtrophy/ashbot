import io
import logging
import random

import aiohttp
import discord
from discord.commands import SlashCommandGroup
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class FakeThing(commands.Cog):
    '''Implements requestable AI generated image commands.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_command_error(self, ctx: discord.ApplicationContext, error):
        logger.error(f'WebRequest error: {error}')
        if isinstance(error, commands.CommandOnCooldown):
            return await ctx.respond(f'you are trying to make that request too fast! please wait another {error.retry_after:.2f}s', ephemeral=True)
        # TODO: catch other errors like from aiohttp here?
        await ctx.respond(f'something went wrong while executing that command! `{error}`', ephemeral=True)

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        self.client = aiohttp.ClientSession()

    async def cog_after_invoke(self, ctx: discord.ApplicationContext):
        await self.client.close()

    fake = SlashCommandGroup('fake', 'AI generated images')

    def generate_embed(self, thing: str):
        phrases = [
            'heres your {thing}!',
            'you wanted a {thing}?',
            'got a {thing} for you',
            'i got a {thing} for u',
            'i gots a {thing} for you!',
            '{thing}, for you.',
        ]
        phrase = random.choice(phrases).format(thing=thing)
        if thing in ('cat', 'fursona'):
            add_on = random.choice(['', ':3', 'uwu', 'UwU'])
            phrase += f' {add_on}' if add_on != '' else ''

        e = discord.Embed(
            title=phrase,
            color=discord.Color.random(),
        )
        e.set_image(url=f'attachment://fake_{thing}.png')
        return e

    def generate_hex_string(self, sliders: int):
        hex_str = ''
        its = 0

        while len(hex_str) != sliders * 2:
            hex_str = ''
            for _ in range(sliders):
                h = hex(random.randint(0, 255))[2:]
                hex_str += f'0{h}' if len(h) == 1 else h

            its += 1
            if its > 50:
                raise Exception('Failed to generate a valid hex string!')

        return hex_str

    # TODO: could prob either move more code here to cog_before_invoke to help cut down on repetition, but there might still be some kinks idk
    #       if not cog_before_invoke, then create other functions to reduce repeated code

    # TODO: add randomly generated cat names? :3
    @fake.command()
    @commands.cooldown(rate=1, per=5)
    async def cat(self, ctx: discord.ApplicationContext):
        '''Sends an AI generated cat from thiscatdoesnotexist.com'''
        async with self.client.get('https://thiscatdoesnotexist.com/') as r:
            if r.status != 200:
                return await ctx.respond(f'sorry, no cat rn, something came up... (if you care, tell the dev the error you got was "`HTTP {r.status}`")', ephemeral=True)
            data = await r.read()
            file = discord.File(
                io.BytesIO(data),
                filename='fake_cat.png',
                description='AI-generated cat, courtesy of thiscatdoesnotexist.com',
            )

        embed = self.generate_embed('cat')
        await ctx.respond(embed=embed, file=file)

    # TODO: add option to get extra generated details from that website that creates fake personal details too
    @fake.command()
    @commands.cooldown(rate=1, per=5)
    async def person(self, ctx: discord.ApplicationContext):
        '''Sends an AI generated person from thispersondoesnotexist.com'''
        async with self.client.get('https://thispersondoesnotexist.com/image') as r:
            if r.status != 200:
                return await ctx.respond(f'sorry, no people were available. (error message for the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.read()
            file = discord.File(
                io.BytesIO(data),
                filename='fake_person.png',
                description='AI-generated person, courtesy of thispersondoesnotexist.com',
            )

        embed = self.generate_embed('person')
        await ctx.respond(embed=embed, file=file)

    @fake.command()
    @commands.cooldown(rate=1, per=5)
    async def garfield(self, ctx: discord.ApplicationContext):
        '''Sends an AI generated Garfield comic strip.'''
        hex_string = self.generate_hex_string(30)

        async with self.client.get(f'http://codeparade.net/garfield/gen_{hex_string}.jpg') as r:
            if r.status != 200:
                return await ctx.respond(f'oops! no garfield comics. (tell the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.read()
            file = discord.File(
                io.BytesIO(data),
                filename='fake_garfield.png',
                description='AI-generated garfield comic, courtesy of codeparade.net/garfield',
            )

        embed = self.generate_embed('garfield')
        await ctx.respond(embed=embed, file=file)

    @fake.command()
    @commands.cooldown(rate=1, per=5)
    async def fursona(self, ctx: discord.ApplicationContext):
        '''Sends an AI generated nightma.. err, fursona.'''
        hex_string = self.generate_hex_string(32)

        async with self.client.get(f'http://codeparade.net/furry/gen_{hex_string}.jpg') as r:
            if r.status != 200:
                return await ctx.respond(f';w; the fuwsona stowe was aww out 3: (pwease teww the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.read()
            file = discord.File(
                io.BytesIO(data),
                filename='fake_fursona.png',
                description='AI-generated fursonas, courtesy of codeparade.net/furry',
            )

        embed = self.generate_embed('fursona')
        await ctx.respond(embed=embed, file=file)

def setup(bot: discord.Bot):
    bot.add_cog(FakeThing(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('FakeThing')
