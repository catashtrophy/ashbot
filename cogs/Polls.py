from typing import Optional, Sequence

import discord
from discord.commands import Option, slash_command
from discord.ext import commands

# TODO: finish and test
class PollModal(discord.ui.Modal):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.add_item(discord.ui.InputText(
            label='Poll Title',
            placeholder='my very cool poll',
        ))
        self.add_item(discord.ui.InputText(
            label='Poll Options (one per line, up to 5)',
            style=discord.InputTextStyle.long,
            placeholder='option 1\noption 2\noption 3\n...',
        ))
        # TODO: this one may need some rework idk
        self.add_item(discord.ui.InputText(
            label='Poll Option Emojis (optional! one per line, up to 5)',
            style=discord.InputTextStyle.long,
            placeholder='😐\n😍\n😋\n...',
            required=False,
        ))
        self.add_item(discord.ui.InputText(
            label='Poll Timeout (seconds passed from last vote until poll closes)',
            placeholder='600',
            value='600',
            max_length=4,
        ))

    async def callback(self, interaction: discord.Interaction):
        # "parse" poll title
        title = self.children[0].value

        # set up poll response embed
        embed = discord.Embed(
            color=discord.Color.random(),
            title=self.children[0].value,
            timestamp=discord.utils.utcnow(),
        )
        embed.set_author(name=str(interaction.user), icon_url=interaction.user.avatar.url)

        # TODO: parse options and option emojis
        options = [
            'one',
            'two',
            'three',
            'four',
            'five',
        ]
        option_emojis = [
            '😐',
            '😍',
            '😋',
            None,
            None,
        ]

        # TODO: parse poll timeout
        timeout = 600

        # create poll response view with poll buttons
        class PollView(discord.ui.View):
            def __init__(self):
                super().__init__(timeout=timeout)
                self.votes = {
                    options[0]: [],
                    options[1]: [],
                    options[2]: [],
                    options[3]: [],
                    options[4]: [],
                }

            def id_already_voted(self, id: int) -> Optional[int]:
                if id in self.votes[options[0]]:
                    return 0
                if id in self.votes[options[1]]:
                    return 1
                if id in self.votes[options[2]]:
                    return 2
                if id in self.votes[options[3]]:
                    return 3
                if id in self.votes[options[4]]:
                    return 4
                return None

            async def on_timeout(self):
                # tally votes? or do somewhere else where i can actually use an interaction
                pass

            @discord.ui.button(label=options[0], style=discord.ButtonStyle.blurple, emoji=option_emojis[0])
            async def opt1_callback(self, button: discord.ui.Button, int: discord.Interaction):
                voted = self.id_already_voted(int.user.id)
                if voted is None:
                    self.votes[options[0]].append(int.user.id)
                    await int.response.send_message(f'you successfully cast your vote for "{options[0]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)
                elif voted == 0:
                    self.votes[options[0]].remove(int.user.id)
                    await int.response.send_message(f'you uncast your vote for "{options[0]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)

            @discord.ui.button(label=options[1], style=discord.ButtonStyle.blurple, emoji=option_emojis[1])
            async def opt1_callback(self, button: discord.ui.Button, int: discord.Interaction):
                voted = self.id_already_voted(int.user.id)
                if voted is None:
                    self.votes[options[1]].append(int.user.id)
                    await int.response.send_message(f'you successfully cast your vote for "{options[1]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)
                elif voted == 1:
                    self.votes[options[1]].remove(int.user.id)
                    await int.response.send_message(f'you uncast your vote for "{options[1]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)

            @discord.ui.button(label=options[2], style=discord.ButtonStyle.blurple, emoji=option_emojis[2])
            async def opt1_callback(self, button: discord.ui.Button, int: discord.Interaction):
                voted = self.id_already_voted(int.user.id)
                if voted is None:
                    self.votes[options[2]].append(int.user.id)
                    await int.response.send_message(f'you successfully cast your vote for "{options[2]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)
                elif voted == 2:
                    self.votes[options[2]].remove(int.user.id)
                    await int.response.send_message(f'you uncast your vote for "{options[2]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)

            @discord.ui.button(label=options[3], style=discord.ButtonStyle.blurple, emoji=option_emojis[3])
            async def opt1_callback(self, button: discord.ui.Button, int: discord.Interaction):
                voted = self.id_already_voted(int.user.id)
                if voted is None:
                    self.votes[options[3]].append(int.user.id)
                    await int.response.send_message(f'you successfully cast your vote for "{options[3]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)
                elif voted == 3:
                    self.votes[options[3]].remove(int.user.id)
                    await int.response.send_message(f'you uncast your vote for "{options[3]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)

            @discord.ui.button(label=options[4], style=discord.ButtonStyle.blurple, emoji=option_emojis[4])
            async def opt1_callback(self, button: discord.ui.Button, int: discord.Interaction):
                voted = self.id_already_voted(int.user.id)
                if voted is None:
                    self.votes[options[4]].append(int.user.id)
                    await int.response.send_message(f'you successfully cast your vote for "{options[4]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)
                elif voted == 4:
                    self.votes[options[4]].remove(int.user.id)
                    await int.response.send_message(f'you uncast your vote for "{options[4]}" in the poll titled "{title}"', ephemeral=True, delete_after=10.0)

        await interaction.response.send_message(embed=embed, view=PollView())

class Polls(commands.Cog):
    '''Implements polling functionality with modals and buttons and all that nice shit.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    # TODO: finish and test
    @slash_command()
    async def quickpoll(self, ctx: discord.ApplicationContext):
        '''Pops up the form for creating a quickpoll in the current channel.'''
        modal = PollModal(title='Quickpoll Creation Form')
        # await ctx.send_modal(modal) # not working? no fucking clue
        await ctx.respond('fuck')

def setup(bot: discord.Bot):
    bot.add_cog(Polls(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('Polls')
