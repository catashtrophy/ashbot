import logging
import random
from typing import Optional

import demoji
import discord
from discord.commands import  SlashCommandGroup, Option, permissions
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class RandomReacts(commands.Cog):
    '''Implements random reacts-related commands for bot moderators/server owners.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        logger.info(f'{ctx.author} executed (likely) priveleged random_reacts command: `{ctx.command.qualified_name}`')

        self.db = self.bot.get_cog('Database')
        if self.db is None:
            raise Exception('Database cog not loaded!')

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        db = self.bot.get_cog('Database')
        if db is None:
            logger.error('Database cog not loaded for random reacts!')
            return

        # guild/bot check
        if not message.guild or message.author.bot:
            return

        # random_react enable check
        random_react_chance = await db.random_reacts_enabled(message.guild.id)
        if random_react_chance is None:
            return

        # random reaction chance check
        if random.uniform(0, 1) > random_react_chance:
            return

        # random reaction
        reactions = await db.get_random_reactions(message.guild.id, message.author.id)
        if not reactions:
            return
        reaction = random.choice(reactions)
        await message.add_reaction(reaction)

    # TODO: dog shit permissions broken wait for fix (see Admin.py notes on same issue for more info)
    random_reacts = SlashCommandGroup(
        'random_reacts',
        'Enable/disable random reacts for the guild, get or set the random react chance.',
        # permissions=[
        #     permissions.CommandPermission(id='mod', type=1, permission=True),
        #     permissions.CommandPermission(id='moderator', type=1, permission=True),
        #     permissions.CommandPermission(id='admin', type=1, permission=True),
        #     permissions.CommandPermission(id='administrator', type=1, permission=True),
        #     permissions.CommandPermission(id='op', type=1, permission=True),
        #     permissions.CommandPermission(id='operator', type=1, permission=True),
        # ],
        # default_permission=False,
    )

    @random_reacts.command()
    async def enable(
        self, ctx: discord.ApplicationContext,
        chance: Option(
            float,
            'Chance for a random react to occur',
            min_value=0.0,
            max_value=1.0,
            required=False,
        ),
    ):
        '''Enable random reacts for the guild, optionally setting the random react chance as well.'''
        await self.db.enable_random_reacts(ctx.guild_id, chance=chance)

        chance_percent = chance * 100 if chance is not None else 0
        chance_text = f' also set the random react chance to {chance_percent:.3f}%' if chance is not None else ''
        await ctx.respond(f'successfully enabled random reacts for the guild.{chance_text}', ephemeral=True)

    @random_reacts.command()
    async def disable(self, ctx: discord.ApplicationContext):
        '''Disable random reacts for the guild.'''
        await self.db.disable_random_reacts(ctx.guild_id)
        await ctx.respond(f'successfully disabled random reacts for the guild.', ephemeral=True)

    @random_reacts.command()
    async def check(self, ctx: discord.ApplicationContext):
        '''Checks the status of random reacts for the guild.'''
        chance = await self.db.random_reacts_enabled(ctx.guild_id)
        if chance is None:
            return await ctx.respond(f'Random reacts are currently disabled for this guild.', ephemeral=True)

        await ctx.respond(f'Random reacts are currently enabled, and the chance of a random react occuring is {chance * 100:.3f}%.', ephemeral=True)

    @random_reacts.command()
    async def set_chance(
        self, ctx: discord.ApplicationContext,
        chance: Option(
            float,
            'Chance for a random react to occur',
            min_value=0.0,
            max_value=1.0,
        ),
    ):
        '''Sets the random react chance for the guild.'''
        await self.db.set_random_reacts_chance(ctx.guild_id, chance)
        await ctx.respond(f'successfully set the chance of a random react occuring in this guild to {chance * 100:.3f}%.', ephemeral=True)

    # no permissions here, meant for everyone :3
    reactions = SlashCommandGroup(
        'reactions',
        'Add/get/remove random reactions for yourself.'
    )

    def parse_input_emoji(self, emoji: str) -> Optional[discord.PartialEmoji]:
        '''Takes the input string and tries to get a valid PartialEmoji out of it. Returns None on failure'''
        # if a custom emoji is contained in the sent emoji string, but it isn't the first thing in the message
        if not emoji.startswith('<') and '<' in emoji:
            # trim input string up to the beginning of the custom emoji
            emoji = emoji[emoji.index('<'):]

        p_emoji = discord.PartialEmoji.from_str(emoji)

        # if custom emoji isnt in current guild, cant add
        if p_emoji.is_custom_emoji() and self.bot.get_emoji(p_emoji.id) is None:
            return None

        # parse out unicode emoji
        if p_emoji.is_unicode_emoji():
            detected = demoji.findall_list(emoji, desc=False)
            if len(detected) < 1:
                return None
            # reparse the PartialEmoji from the first detected emoji
            p_emoji = discord.PartialEmoji.from_str(detected[0])

        return p_emoji

    @reactions.command()
    async def add(
        self, ctx: discord.ApplicationContext,
        emoji: Option(str, 'The emoji you would like to add to your random reaction list. MUST be unicode OR from this guild!'),
    ):
        '''Adds an emoji to your random reaction list.'''
        p_emoji = self.parse_input_emoji(emoji)
        if p_emoji is None:
            return await ctx.respond(f'could not set "{emoji}" as a random react for you, sorry :(', ephemeral=True)

        await self.db.add_random_reaction(ctx.guild_id, ctx.author.id, p_emoji._as_reaction())
        await ctx.respond(f'added {p_emoji} as a random reaction for you!', ephemeral=True)

    @reactions.command()
    async def get(
        self, ctx: discord.ApplicationContext,
        user: Option(
            discord.Member,
            'Optional user to get reaction list for',
            default=None,
            required=False,
        ),
    ):
        '''Gets your (or another member's) list of random reactions.'''
        user_id = user.id if user is not None else ctx.author.id
        target = f'{user.mention} has' if user is not None else 'you have'
        reactions = await self.db.get_random_reactions(ctx.guild_id, user_id)
        emojis = [str(discord.PartialEmoji.from_str(r)) for r in reactions]
        await ctx.respond(f'{target} the following random reactions stored: {", ".join(emojis) if emojis else "oh, none!"}', ephemeral=True)

    @reactions.command()
    async def remove(
        self, ctx: discord.ApplicationContext,
        emoji: Option(str, 'The emoji you would like to remove from your random reaction list'),
    ):
        '''Removes the given emoji from your list of random reactions.'''
        p_emoji = self.parse_input_emoji(emoji)
        if p_emoji is None:
            return await ctx.respond(f"{emoji} isn't a valid random reaction for this guild, so it couldn't have been set.", ephemeral=True)

        reactions = await self.db.get_random_reactions(ctx.guild_id, ctx.author.id)
        if p_emoji._as_reaction() not in reactions:
            return await ctx.respond(f'{p_emoji} was not in your list of reactions. want to try adding it instead?', ephemeral=True)

        removed = await self.db.remove_random_reaction(ctx.guild_id, ctx.author.id, p_emoji._as_reaction())
        if not removed:
            return await ctx.respond(f'failed to remove {p_emoji} from your random reactions list! thats as helpful as i can be, unfortuantely', ephemeral=True)
        await ctx.respond(f'removed {p_emoji} from your random reactions list.', ephemeral=True)

def setup(bot: discord.Bot):
    bot.add_cog(RandomReacts(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('RandomReacts')
