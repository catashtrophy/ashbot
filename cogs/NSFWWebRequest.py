import logging
import random

import aiohttp
import discord
from discord.commands import Option, slash_command
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class NSFWWebRequest(commands.Cog):
    '''Implements various *naughty* web requesting commands for adults only.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_command_error(self, ctx: discord.ApplicationContext, error):
        logger.error(f'NSFWWebRequest error: {error}')
        if isinstance(error, commands.CommandOnCooldown):
            return await ctx.respond(f'you are trying to make that request too fast! please wait another {error.retry_after:.2f}s', ephemeral=True)
        if isinstance(error, commands.NSFWChannelRequired):
            return await ctx.respond(f"you can't do that in this channel! ({error})", ephemeral=True)
        # TODO: catch other errors like from aiohttp here?
        await ctx.respond(f'something went wrong while executing that command! `{error}`', ephemeral=True)

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        self.client = aiohttp.ClientSession()

    async def cog_after_invoke(self, ctx: discord.ApplicationContext):
        await self.client.close()

    # TODO: mark embed spoiler? might need to download image, add as file attachment, and mark the file as spoiler
    @slash_command()
    @commands.cooldown(rate=1, per=5)
    @commands.is_nsfw()
    async def e621(
        self, ctx: discord.ApplicationContext,
        tags: Option(str, 'Each tag you want to add to your search query, **separated by space**')
    ):
        '''Gets a post from e621 that contains all of your searched tags.'''
        headers = { 'User-Agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36' }
        params = {
            'tags': tags,
            'limit': 25,
        }

        async with self.client.get('https://e621.net/posts.json', headers=headers, params=params) as r:
            if r.status != 200:
                return await ctx.respond(f';w; sumting went wwong wif yuww wequest (error for the dev: "`HTTP {r.status}`")', ephemeral=True)
            data = await r.json()
            if not data['posts'] or not len(data['posts']):
                return await ctx.respond(f'no resuwts found fow `{tags.replace(" ", "`, `")}`', ephemeral=True) # TODO: make not ephemeral?
            result = random.choice(data['posts'])

        embed = discord.Embed(
            color=discord.Color.random(),
            description=result['description'],
            timestamp=discord.utils.parse_time(result['updated_at']),
            title=f"`{tags.replace(' ', '`, `')}`",
            url=f"https://e926.net/posts/{result['id']}?q={tags.replace(' ', '+')}",
        )
        embed.set_author(name=', '.join(result['tags']['artist']))
        embed.set_footer(text=f'score: {result["score"]["total"]}')
        embed.set_image(url=result['file']['url'])
        embed.set_thumbnail(url=result['preview']['url'])
        for tag_cat, tag_list in result['tags'].items():
            if tag_cat == 'artist': continue
            if not len(tag_list): continue
            joined_tags = ', '.join(tag_list)
            # stop loop when next embed breaks size limit
            if len(joined_tags) + len(embed) > 6000: break
            embed.add_field(name=f'{tag_cat.title()} tags', value=joined_tags, inline=False)

        await ctx.respond(embed=embed)

    # TODO: r34 impl? which site? prob rewrite in python rather than try to port across as is, bc it was weird

def setup(bot: discord.Bot):
    bot.add_cog(NSFWWebRequest(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('NSFWWebRequest')
