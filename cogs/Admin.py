import logging

import discord
from discord.commands import SlashCommandGroup, Option, permissions
from discord.ext import commands

logging.getLogger('ashbot').addHandler(logging.NullHandler())
logger = logging.getLogger('ashbot')

class Admin(commands.Cog):
    '''Implements priveleged commands for the bot administrator to use.'''
    def __init__(self, bot: discord.Bot):
        self.bot = bot

    async def cog_before_invoke(self, ctx: discord.ApplicationContext):
        logger.warning(f'{ctx.author} executed priveleged admin command: `{ctx.command.qualified_name}`')

        self.db = self.bot.get_cog('Database')
        if self.db is None:
            raise Exception('Database cog not loaded!')

    # TODO: pycord permissions are broken as of new discord permissions API impl,
    #       waiting on update. until then, oops these commands are public hehe
    admin = SlashCommandGroup(
        'admin',
        'Administrative bot commands.',
        # permissions=[permissions.CommandPermission(id='owner', type=2, permission=True)],
        # default_permission=False,
    )

    @admin.command()
    async def reload(
        self, ctx: discord.ApplicationContext,
        cog: Option(str, 'Optional cog name to be reloaded (NAME only, no qualifier)', default=None),
    ):
        '''Reloads all bot extensions.'''
        cogs = {
            'clean': [],
            'dirty': [],
        }

        exts = self.bot.extensions.copy() if cog is None else [f'cogs.{cog}']
        for ext in exts:
            try:
                self.bot.reload_extension(ext)
                cogs['clean'].append(ext)
            except discord.DiscordException as exc:
                logger.error(f'Failed to reload extension "{ext}"! {exc.__class__.__name__}: {exc}')
                cogs['dirty'].append(ext)

        if len(cogs['dirty']):
            if not len(cogs['clean']):
                await ctx.respond(f'failed to reload extension(s)! list of failed extensions: `{"`, `".join(cogs["dirty"])}`', ephemeral=True)
            else:
                reload_list = [f"{'✔' if ext in cogs['clean'] else '✖'} `{ext}`" for ext in exts]
                await ctx.respond('\n'.join(reload_list) or 'no extensions to reload!', ephemeral=True)
        else:
            if cog is not None:
                await ctx.respond(f'successfully reloaded {cog}!', ephemeral=True)
            else:
                await ctx.respond(f'successfully reloaded all extensions!', ephemeral=True)

    @admin.command()
    async def say(
        self, ctx: discord.ApplicationContext,
        message: Option(str, 'Message to be sent'),
        tts: Option(bool, 'Send message using text-to-speech', default=False),
    ):
        '''Sends a message in the current channel as the bot.'''
        await ctx.interaction.channel.send(content=message, tts=tts)
        await ctx.respond('your message has been delivered :3', ephemeral=True)

    @admin.command()
    async def add_slap(
        self, ctx: discord.ApplicationContext,
        weapon: Option(str, 'The weapon used for the slap'),
        action: Option(str, 'Optional action, if not "slapped"', required=False, default=None),
        flavor_text: Option(str, 'Optional flavor text for the slap', required=False, default=None),
        for_bots: Option(bool, 'If this slap weapon is intended to be used on bots', default=False),
    ):
        '''Adds a slap weapon to the slap_weapons table.'''
        slap_id = await self.db.add_slap_weapon(weapon, action=action, flavor_text=flavor_text, for_bots=for_bots)
        if slap_id is None:
            return await ctx.respond('failed to add that slap weapon!', ephemeral=True)

        sample_slap = '{user} {action} {target}{flavor_text} with **{weapon}**'.format(
            user=ctx.bot.user.mention,
            action='slapped' if action is None else action,
            target=ctx.user.mention,
            flavor_text='' if flavor_text is None else f' {flavor_text}',
            weapon=weapon,
        )
        await ctx.respond(f'slap created with id: `{slap_id}`! Have a sample:\n{sample_slap}', ephemeral=True)

    @admin.command()
    async def get_slaps(self, ctx: discord.ApplicationContext):
        '''Gets a list of all slap weapons from the slap_weapons table'''
        slap_list = await self.db.get_slap_weapons()
        slaps = [f'`{s[0]}`: "{s[1]}"{" (bots only)" if s[4] == "Y" else ""}' for s in slap_list]
        if not len(slaps):
            return await ctx.respond('no slaps are currently configured! try adding some, instead.', ephemeral=True)
        await ctx.respond(', '.join(slaps), ephemeral=True)

    @admin.command()
    async def remove_slap(
        self, ctx: discord.ApplicationContext,
        id: Option(int, 'The id of the slap you wish to remove from the slap_weapons table'),
    ):
        '''Removes the slap weapon with the given id from the slap_weapons table'''
        slap_list = await self.db.get_slap_weapon_ids()
        if not (id,) in slap_list:
            return await ctx.respond(f'slap weapon with id `{id}` does not exist!', ephemeral=True)

        removed = await self.db.remove_slap_weapon(id)
        if not removed:
            return await ctx.respond(f'failed to remove slap weapon with id `{id}`, probably does not exist in table.', ephemeral=True)

        await ctx.respond(f'successfully removed slap weapon with id `{id}`.', ephemeral=True)

    @admin.command()
    async def test(self, ctx: discord.ApplicationContext):
        '''Test command for testing purposes.'''
        await ctx.respond(f'ping time: `{self.bot.latency * 100:.4f}ms`', ephemeral=True)

def setup(bot: discord.Bot):
    bot.add_cog(Admin(bot))

def teardown(bot: discord.Bot):
    bot.remove_cog('Admin')
